import java.util.Scanner;

import java.util.*;

public class User {
	Scanner sc = new Scanner(System.in);
	
	public User() {
		super();
		System.out.println("Enter User name");
		userName=sc.next();
		System.out.println("Enter User E-mail");
		email=sc.next();
		
	}
	String userName; 
	String email;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
