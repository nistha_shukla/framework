
public class Request {
	User[] user=new User[2];
	HTMLParser parser;

public Request(String url, String dataType, String dataHeader) {
		super();
		this.url = url;
		this.dataType = dataType;
		this.dataHeader = dataHeader;
		for(int i=0; i<2; i++)
		{
			user[i++]=new User();
		}
		parser=new HTMLParser(url);
		
	}

String url;
String dataType;
String dataHeader;
String text;
public User[] getUser() {
	return user;
}
public void setUser(User[] user) {
	this.user = user;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getDataType() {
	return dataType;
}
public void setDataType(String dataType) {
	this.dataType = dataType;
}
public String getDataHeader() {
	return dataHeader;
}
public void setDataHeader(String dataHeader) {
	this.dataHeader = dataHeader;
}
public String getText() {
	return text;
}
public void setText(String text) {
	this.text = text;
}

}
