
import java.io.File;
import java.io.IOException;
 

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class HTMLParser{
 public HTMLParser(String url) {
		super();
		this.url = url;
		pmain();
	}
static String url;
    public void pmain() {
        String h2="";
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
            String title = doc.title();
             h2 = doc.body().getElementsByTag("p").text();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        System.out.println("Jsoup Can read HTML page from URL, title : " + h2);
        
 
//       //  JSoup Example 3 - Parsing an HTML file in Java
//        Document htmlFile = Jsoup.parse("login.html", "ISO-8859-1"); // wrong
//      //  Document htmlFile = null;
//        try {
//        	
//            htmlFile = Jsoup.parse(new File("login.html"), "ISO-8859-1");
//
//        	
//        } 
//        catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } // right
//        title = htmlFile.title();
//        Element div = htmlFile.getElementById("login");
//        //String cssClass = div.className(); // getting class form HTML element
// 
//        System.out.println("Jsoup can also parse HTML file directly");
//        System.out.println("title : " + title);
//        //System.out.println("class of div tag : " + cssClass);
//    }
    }
}